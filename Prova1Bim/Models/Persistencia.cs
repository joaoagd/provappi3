﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;

namespace Prova1Bim.Models
{
    public class Persistencia
    {
        MySqlConnection _conexao;

        MySqlCommand _cmd ;
        bool _fecharConexaoAutomaticamente = true;


        public Persistencia(bool fecharConexaoAutomaticamente = true)
        {
            _fecharConexaoAutomaticamente = fecharConexaoAutomaticamente;
            _conexao = new MySqlConnection(@"Server=mysql1.gear.host;Database=prova1bim;User Id=prova1bim;Password=	123456!;");
            _cmd = _conexao.CreateCommand();
        }

        public void AbrirConexao()
        {
            if(_conexao.State != System.Data.ConnectionState.Open)
            {
                _conexao.Open();
            }
        }
        
        public void FecharConexao()
        {
            _conexao.Close();
        }

        public int ExecuteNonQuery(string sql, List<MySqlParameter> parametros = null)
        {
             int linhasAfetadas = 0;
            _cmd.CommandText = sql;

            AbrirConexao();
            
            linhasAfetadas = _cmd.CommandText.Count();

            if (_fecharConexaoAutomaticamente)
                FecharConexao();

            return linhasAfetadas;
        }


        public Int64 ExecuteNonQueryLastKey(string sql, List<MySqlParameter> parametros = null)
        {
            Int64 lastId = 0;
            _cmd.CommandText = sql;

            if (parametros != null)
                _cmd.Parameters.AddRange(parametros.ToArray());

            AbrirConexao();

            //segundo erro aqui?
            _cmd.ExecuteNonQuery();

            lastId = _cmd.LastInsertedId;
            if (_fecharConexaoAutomaticamente)
                FecharConexao();

            return lastId;
        }

        public DataTable ExecutaSelect(string select, List<MySqlParameter> parametros = null)
        {
            DataTable dt = new DataTable();
            _cmd.CommandText = select;

            if (parametros != null)
                _cmd.Parameters.AddRange(parametros.ToArray());

            AbrirConexao();
            dt.Load(_cmd.ExecuteReader());

            if (_fecharConexaoAutomaticamente)
                FecharConexao();

            return dt;
        }

   

        public Object ExecutaScalar(string sql, List<MySqlParameter> parametros = null)
        {
            object retorno = null;
            _cmd.CommandText = sql;

            if (parametros != null)
                _cmd.Parameters.AddRange(parametros.ToArray());

            AbrirConexao();
            retorno = _cmd.ExecuteScalar();

            if (_fecharConexaoAutomaticamente)
                FecharConexao();

            return retorno;
        }

        public void AdicionarParametro(string nome, object valor)
        {
            _cmd.Parameters.AddWithValue(nome, valor);
        }

    }
}